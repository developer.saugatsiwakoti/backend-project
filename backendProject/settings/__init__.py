import os
from django.core.exceptions import ImproperlyConfigured

backend_project_mode = os.environ.get('MODE')


# check if the app mode is scpecified
if not backend_project_mode:
    raise ImproperlyConfigured("No mode specified for project")

# check if mode is in dev and run it with development settings and modules
if backend_project_mode == 'DEV' or backend_project_mode == 'dev':
    from backendProject.settings.dev import *

# check if mode is in prod and run it with the production settings and modules
elif backend_project_mode == 'PROD' or backend_project_mode == 'prod':
    from backendProject.settings.prod import *

# check if mode is not dev or prod
else:
    raise ImproperlyConfigured(
        "Please specify correct mode in your env file MODE=DEV||prod or MODE=PROD||prod")
